import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import { HamburgerCollapseReverse } from "react-animated-burgers";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNav: false,
    };
  }

  componentDidMount = () => {};
  showHamburgerMenu = (status) => {
    this.setState({ showNav: status });
  };

  render() {
    return (
      <React.Fragment>
       <nav className="navbar navbar-expand-lg py-lg-3 navbar-dark">
            <div className="container">

                <a href="index.html" className="navbar-brand mr-lg-5">
                    <img src="assets/images/logo.png" alt="" className="logo-dark" height="18"/>
                </a>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <i className="mdi mdi-menu"></i>
                </button>

                <div className="collapse navbar-collapse" id="navbarNavDropdown">

                    <ul className="navbar-nav mr-auto align-items-center">
                        <li className="nav-item mx-lg-1">
                            <a className="nav-link active" href="">Home</a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a className="nav-link" href="">Features</a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a className="nav-link" href="">Pricing</a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a className="nav-link" href="">FAQs</a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a className="nav-link" href="">Clients</a>
                        </li>
                        <li className="nav-item mx-lg-1">
                            <a className="nav-link" href="">Contact</a>
                        </li>
                    </ul>
                    <ul className="navbar-nav ml-auto align-items-center">
                        <li className="nav-item mr-0">
                            <a href="https://themes.getbootstrap.com/product/hyper-responsive-admin-dashboard-template/" target="_blank" className="nav-link d-lg-none">Purchase now</a>
                            <a href="https://themes.getbootstrap.com/product/hyper-responsive-admin-dashboard-template/" target="_blank" className="btn btn-sm btn-light btn-rounded d-none d-lg-inline-flex">
                                <i className="mdi mdi-basket mr-2"></i> Purchase Now
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
      </React.Fragment>
    );
  }
}

export default NavBar;