
import "./styles/main.scss";
import Navbar from "./components/navbar";

function App() {
  return (
    <div className="App">
    <Navbar/>

    <div className="content"></div>
    </div>
  );
}

export default App;
